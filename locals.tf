locals {
  name_suffix = format("%s.%s.%s", var.project, var.environment, var.unit)
  fqdn_suffix = format("%s.%s", local.name_suffix, var.domain)

  tags = {
    Domain      = var.domain
    Unit        = var.unit
    Environment = var.environment
    Project     = var.project
  }

  labels = {
    domain      = var.domain
    unit        = var.unit
    environment = var.environment
    project     = var.project
  }
}
