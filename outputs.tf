output name_suffix {
  value = local.name_suffix
}

output fqdn_suffix {
  value = local.fqdn_suffix
}

output tags {
  value = local.tags
}

output labels {
  value = local.labels
}
